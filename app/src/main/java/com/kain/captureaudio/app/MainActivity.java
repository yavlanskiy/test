package com.kain.captureaudio.app;

import android.app.Activity;
import android.media.*;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.*;
import java.util.concurrent.TimeUnit;


public class MainActivity extends Activity {

    private MediaRecorder mediaRecorder;
    private MediaPlayer mediaPlayer;
    private String fileName;
    DataInputStream dis;
    AudioRecord audioRecord;
    boolean isRecording = false;

    Button rec;
    Button play;

    TextView tv;
    int frequency = 44100;
    int channelConfiguration = AudioFormat.CHANNEL_IN_MONO;
    int audioEncoding = AudioFormat.ENCODING_PCM_16BIT;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fileName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/record.3gpp";
        rec = (Button) findViewById(R.id.button_rec);
        play = (Button) findViewById(R.id.button_play);
    }

    public void record() {
        File file = new File(fileName);

        if (file.exists())
            file.delete();

        try {
            file.createNewFile();
        } catch (IOException e) {
            throw new IllegalStateException("Failed to create " + file.toString());
        }
        try {
            OutputStream os = new FileOutputStream(file);
            BufferedOutputStream bos = new BufferedOutputStream(os);
            DataOutputStream dos = new DataOutputStream(bos);

            int bufferSize = AudioRecord.getMinBufferSize(frequency, channelConfiguration, audioEncoding);
            AudioRecord audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                    frequency, channelConfiguration,
                    audioEncoding, bufferSize);

            short[] buffer = new short[bufferSize];
            audioRecord.startRecording();


            while (isRecording) {
                int bufferReadResult = audioRecord.read(buffer, 0, bufferSize);
                for (int i = 0; i < bufferReadResult; i++)
                    dos.writeShort(buffer[i]);
            }


            audioRecord.stop();
            audioRecord.release();
            dos.close();

        } catch (Throwable t) {
            Log.e("AudioRecord", "Recording Failed");
        }
    }

    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_rec:
                isRecording = true;
                Thread thread = new Thread(new Runnable() {
                    public void run() {
                        record();
                    }
                });
                thread.start();
                sleapTen();

                isRecording = false;
                break;
            case R.id.button_play:
                Thread t2 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Reverse();
                    }
                });
                t2.start();

                break;
        }
//        isRecording = true;
//        Thread thread = new Thread(new Runnable() {
//            public void run() {
//                record();
//            }
//        });
//        thread.start();
//        sleapTen();
//
//        isRecording = false;
//
    }

    public void Reverse() {

        File file = new File(fileName);
        int musicLength = (int) (file.length());
        short[] music = new short[musicLength];


        try {
            InputStream is = new FileInputStream(file);
            BufferedInputStream bis = new BufferedInputStream(is);
            DataInputStream dis = new DataInputStream(bis);

            int i = 0;
            while (dis.available() > 0) {
                music[musicLength - 1 - i] = dis.readShort();
                i++;
            }
            dis.close();

            AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                    44100,
                    AudioFormat.CHANNEL_OUT_MONO,
                    AudioFormat.ENCODING_PCM_16BIT,
                    musicLength,
                    AudioTrack.MODE_STREAM);
            audioTrack.play();

            audioTrack.write(music, 0, musicLength);
        } catch (Throwable t) {
            Log.e("AudioTrack", "Playback Failed");
        }

    }

    void sleapTen() {
        // пауза - 5 секунд
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        isRecording = false;
        if (audioRecord != null) {
            audioRecord.release();
        }
    }
}
